package com.hsw.springCloud;

import cn.hutool.Hutool;
import cn.hutool.core.util.IdUtil;
import org.junit.Test;

import java.util.Set;

/**
 * @description: TODO
 * @author: com.hsw
 * @date: 2021/3/14
 */
public class test {

    /**
     * hhutool工具包
     */
    @Test
    public void test(){
        Set<Class<?>> allUtils = Hutool.getAllUtils();
        allUtils.forEach(item->{
            item.getAnnotatedInterfaces();
            //System.out.println(item);
        });
        /*
        *   class cn.hutool.core.comparator.CompareUtil
            class cn.hutool.core.math.MathUtil
            class cn.hutool.core.util.CharUtil
            class cn.hutool.core.collection.SpliteratorUtil
            class cn.hutool.core.map.MapUtil
            class cn.hutool.core.io.file.PathUtil
            class cn.hutool.extra.emoji.EmojiUtil
            class cn.hutool.core.util.HashUtil
            class cn.hutool.core.util.HexUtil
            class cn.hutool.core.util.ObjectUtil
            class cn.hutool.core.compiler.DiagnosticUtil
            class cn.hutool.core.collection.CollectionUtil
            class cn.hutool.captcha.CaptchaUtil
            class cn.hutool.http.useragent.UserAgentUtil
            class cn.hutool.extra.template.engine.velocity.VelocityUtil
            class cn.hutool.http.webservice.SoapUtil
            class cn.hutool.core.util.ClassLoaderUtil
            class cn.hutool.db.meta.MetaUtil
            class cn.hutool.core.util.IdUtil
            class cn.hutool.system.SystemUtil
            class cn.hutool.cron.pattern.CronPatternUtil
            class cn.hutool.json.JSONUtil
            class cn.hutool.extra.ssh.JschUtil
            class cn.hutool.core.text.escape.InternalEscapeUtil
            class cn.hutool.core.util.RadixUtil
            class cn.hutool.http.HttpUtil
            class cn.hutool.db.StatementUtil
            class cn.hutool.core.util.TypeUtil
            class cn.hutool.core.util.XmlUtil
            class cn.hutool.core.util.ServiceLoaderUtil
            class cn.hutool.extra.servlet.ServletUtil
            class cn.hutool.core.annotation.AnnotationUtil
            class cn.hutool.core.text.UnicodeUtil
            class cn.hutool.core.io.NioUtil
            class cn.hutool.db.dialect.DriverUtil
            class cn.hutool.dfa.SensitiveUtil
            class cn.hutool.core.util.ArrayUtil
            class cn.hutool.core.img.GraphicsUtil
            class cn.hutool.core.collection.ListUtil
            class cn.hutool.core.util.ClassUtil
            class cn.hutool.core.date.DateUtil
            class cn.hutool.core.util.StrUtil
            class cn.hutool.poi.excel.ExcelFileUtil
            class cn.hutool.extra.template.engine.beetl.BeetlUtil
            class cn.hutool.cron.CronUtil
            class cn.hutool.core.lang.intern.InternUtil
            class cn.hutool.cache.CacheUtil
            class cn.hutool.poi.excel.sax.ExcelSaxUtil
            class cn.hutool.core.util.CharsetUtil
            class cn.hutool.crypto.BCUtil
            class cn.hutool.core.swing.DesktopUtil
            class cn.hutool.crypto.KeyUtil
            class cn.hutool.setting.dialect.PropsUtil
            class cn.hutool.core.collection.IterUtil
            class cn.hutool.core.net.NetUtil
            class cn.hutool.script.ScriptUtil
            class cn.hutool.core.util.ReflectUtil
            class cn.hutool.crypto.SmUtil
            class cn.hutool.extra.pinyin.PinyinUtil
            class cn.hutool.poi.excel.RowUtil
            class cn.hutool.core.compiler.JavaFileObjectUtil
            class cn.hutool.core.swing.clipboard.ClipboardUtil
            class cn.hutool.core.text.csv.CsvUtil
            class cn.hutool.poi.excel.ExcelExtractorUtil
            class cn.hutool.core.img.ImgUtil
            class cn.hutool.poi.excel.WorkbookUtil
            class cn.hutool.core.util.ModifierUtil
            class cn.hutool.extra.qrcode.QrCodeUtil
            class cn.hutool.extra.ssh.GanymedUtil
            class cn.hutool.extra.tokenizer.TokenizerUtil
            class cn.hutool.core.text.CharSequenceUtil
            class cn.hutool.core.util.ZipUtil
            class cn.hutool.core.net.Ipv4Util
            class cn.hutool.poi.excel.ExcelDateUtil
            class cn.hutool.core.compiler.CompilerUtil
            class cn.hutool.core.util.ReferenceUtil
            class cn.hutool.core.io.watch.WatchUtil
            class cn.hutool.crypto.digest.DigestUtil
            class cn.hutool.core.util.RuntimeUtil
            class cn.hutool.extra.spring.SpringUtil
            class cn.hutool.core.net.SSLUtil
            class cn.hutool.core.util.PrimitiveArrayUtil
            class cn.hutool.poi.excel.ExcelPicUtil
            class cn.hutool.core.date.TemporalUtil
            class cn.hutool.core.util.BooleanUtil
            class cn.hutool.extra.mail.MailUtil
            class cn.hutool.core.date.TemporalAccessorUtil
            class cn.hutool.aop.ProxyUtil
            class cn.hutool.setting.SettingUtil
            class cn.hutool.core.thread.lock.LockUtil
            class cn.hutool.bloomfilter.BloomFilterUtil
            class cn.hutool.core.io.resource.ResourceUtil
            class cn.hutool.core.util.URLUtil
            class cn.hutool.core.img.FontUtil
            class cn.hutool.core.util.RandomUtil
            class cn.hutool.core.swing.RobotUtil
            class cn.hutool.core.util.CreditCodeUtil
            class cn.hutool.core.date.CalendarUtil
            class cn.hutool.core.bean.BeanUtil
            class cn.hutool.core.io.file.FileNameUtil
            class cn.hutool.crypto.ECKeyUtil
            class cn.hutool.core.io.unit.DataSizeUtil
            class cn.hutool.socket.SocketUtil
            class cn.hutool.core.io.FileUtil
            class cn.hutool.core.util.EscapeUtil
            class cn.hutool.poi.excel.ExcelUtil
            class cn.hutool.core.util.PageUtil
            class cn.hutool.socket.nio.NioUtil
            class cn.hutool.core.collection.CollUtil
            class cn.hutool.core.util.EnumUtil
            class cn.hutool.poi.word.TableUtil
            class cn.hutool.json.InternalJSONUtil
            class cn.hutool.core.io.FileTypeUtil
            class cn.hutool.extra.validation.ValidationUtil
            class cn.hutool.system.oshi.OshiUtil
            class cn.hutool.poi.word.WordUtil
            class cn.hutool.core.util.ReUtil
            class cn.hutool.core.collection.CollStreamUtil
            class cn.hutool.db.sql.SqlUtil
            class cn.hutool.poi.excel.cell.CellUtil
            class cn.hutool.core.util.IdcardUtil
            class cn.hutool.extra.compress.CompressUtil
            class cn.hutool.core.swing.ScreenUtil
            class cn.hutool.poi.word.DocUtil
            class cn.hutool.extra.mail.InternalMailUtil
            class cn.hutool.crypto.SecureUtil
            class cn.hutool.core.date.LocalDateTimeUtil
            class cn.hutool.extra.cglib.CglibUtil
            class cn.hutool.core.util.NumberUtil
            class cn.hutool.extra.expression.ExpressionUtil
            class cn.hutool.crypto.PemUtil
            class cn.hutool.core.io.BufferUtil
            class cn.hutool.core.lang.caller.CallerUtil
            class cn.hutool.core.thread.ThreadUtil
            class cn.hutool.extra.template.TemplateUtil
            class cn.hutool.http.HtmlUtil
            class cn.hutool.poi.excel.style.StyleUtil
            class cn.hutool.core.exceptions.ExceptionUtil
            class cn.hutool.core.io.IoUtil
            class cn.hutool.core.util.PhoneUtil
            class cn.hutool.core.lang.tree.TreeUtil
            class cn.hutool.db.DbUtil*/
        Hutool.printAllUtils();
        /*
            +－－－－－－－－－－－－－－－－－－－－－－+－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－+
            |　工具类名　　　　　　　　　　　　　　　　　|　所在包　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　　|
            +－－－－－－－－－－－－－－－－－－－－－－+－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－+
            |　ＣｈａｒｓｅｔＵｔｉｌ　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｕｔｉｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＰｉｎｙｉｎＵｔｉｌ　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｅｘｔｒａ．ｐｉｎｙｉｎ　　　　　　　　　　　　　　　　　　　|
            |　ＳｅｒｖｉｃｅＬｏａｄｅｒＵｔｉｌ　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｕｔｉｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＤｒｉｖｅｒＵｔｉｌ　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｄｂ．ｄｉａｌｅｃｔ　　　　　　　　　　　　　　　　　　　　　|
            |　ＳｑｌＵｔｉｌ　　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｄｂ．ｓｑｌ　　　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＬｉｓｔＵｔｉｌ　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｃｏｌｌｅｃｔｉｏｎ　　　　　　　　　　　　　　　　|
            |　ＴｅｍｐｏｒａｌＡｃｃｅｓｓｏｒＵｔｉｌ　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｄａｔｅ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＣａｃｈｅＵｔｉｌ　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃａｃｈｅ　　　　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＥｘｃｅｐｔｉｏｎＵｔｉｌ　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｅｘｃｅｐｔｉｏｎｓ　　　　　　　　　　　　　　　　|
            |　ＢｕｆｆｅｒＵｔｉｌ　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｉｏ　　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＥｓｃａｐｅＵｔｉｌ　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｕｔｉｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＨａｓｈＵｔｉｌ　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｕｔｉｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＮｕｍｂｅｒＵｔｉｌ　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｕｔｉｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＣｈａｒＵｔｉｌ　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｕｔｉｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＣｏｍｐｉｌｅｒＵｔｉｌ　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｃｏｍｐｉｌｅｒ　　　　　　　　　　　　　　　　　　|
            |　ＬｏｃａｌＤａｔｅＴｉｍｅＵｔｉｌ　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｄａｔｅ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＩｍｇＵｔｉｌ　　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｉｍｇ　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＳＳＬＵｔｉｌ　　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｎｅｔ　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＳｍＵｔｉｌ　　　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｒｙｐｔｏ　　　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＣｈａｒＳｅｑｕｅｎｃｅＵｔｉｌ　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｔｅｘｔ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＩｎｔｅｒｎＵｔｉｌ　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｌａｎｇ．ｉｎｔｅｒｎ　　　　　　　　　　　　　　　|
            |　ＳｅｎｓｉｔｉｖｅＵｔｉｌ　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｄｆａ　　　　　　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＣｒｏｎＵｔｉｌ　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｒｏｎ　　　　　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＳｙｓｔｅｍＵｔｉｌ　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｓｙｓｔｅｍ　　　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＴｒｅｅＵｔｉｌ　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｌａｎｇ．ｔｒｅｅ　　　　　　　　　　　　　　　　　|
            |　ＲｅｓｏｕｒｃｅＵｔｉｌ　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｉｏ．ｒｅｓｏｕｒｃｅ　　　　　　　　　　　　　　　|
            |　ＮｉｏＵｔｉｌ　　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｓｏｃｋｅｔ．ｎｉｏ　　　　　　　　　　　　　　　　　　　　　|
            |　ＩｄｃａｒｄＵｔｉｌ　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｕｔｉｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＨｔｍｌＵｔｉｌ　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｈｔｔｐ　　　　　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＣｒｏｎＰａｔｔｅｒｎＵｔｉｌ　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｒｏｎ．ｐａｔｔｅｒｎ　　　　　　　　　　　　　　　　　　　|
            |　ＮｅｔＵｔｉｌ　　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｎｅｔ　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＣｏｌｌＳｔｒｅａｍＵｔｉｌ　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｃｏｌｌｅｃｔｉｏｎ　　　　　　　　　　　　　　　　|
            |　ＢｅａｎＵｔｉｌ　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｂｅａｎ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＰｒｉｍｉｔｉｖｅＡｒｒａｙＵｔｉｌ　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｕｔｉｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＣａｌｌｅｒＵｔｉｌ　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｌａｎｇ．ｃａｌｌｅｒ　　　　　　　　　　　　　　　|
            |　ＰｈｏｎｅＵｔｉｌ　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｕｔｉｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＳｔｒＵｔｉｌ　　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｕｔｉｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＣｏｌｌＵｔｉｌ　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｃｏｌｌｅｃｔｉｏｎ　　　　　　　　　　　　　　　　|
            |　ＧｒａｐｈｉｃｓＵｔｉｌ　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｉｍｇ　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＶｅｌｏｃｉｔｙＵｔｉｌ　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｅｘｔｒａ．ｔｅｍｐｌａｔｅ．ｅｎｇｉｎｅ．ｖｅｌｏｃｉｔｙ　|
            |　ＲｕｎｔｉｍｅＵｔｉｌ　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｕｔｉｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＳｏａｐＵｔｉｌ　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｈｔｔｐ．ｗｅｂｓｅｒｖｉｃｅ　　　　　　　　　　　　　　　　|
            |　ＩｏＵｔｉｌ　　　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｉｏ　　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＰｒｏｐｓＵｔｉｌ　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｓｅｔｔｉｎｇ．ｄｉａｌｅｃｔ　　　　　　　　　　　　　　　　|
            |　ＣｏｍｐｒｅｓｓＵｔｉｌ　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｅｘｔｒａ．ｃｏｍｐｒｅｓｓ　　　　　　　　　　　　　　　　　|
            |　ＥｘｐｒｅｓｓｉｏｎＵｔｉｌ　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｅｘｔｒａ．ｅｘｐｒｅｓｓｉｏｎ　　　　　　　　　　　　　　　|
            |　ＭａｔｈＵｔｉｌ　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｍａｔｈ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＢＣＵｔｉｌ　　　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｒｙｐｔｏ　　　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＢｅｅｔｌＵｔｉｌ　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｅｘｔｒａ．ｔｅｍｐｌａｔｅ．ｅｎｇｉｎｅ．ｂｅｅｔｌ　　　　|
            |　ＣｇｌｉｂＵｔｉｌ　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｅｘｔｒａ．ｃｇｌｉｂ　　　　　　　　　　　　　　　　　　　　|
            |　ＤｉａｇｎｏｓｔｉｃＵｔｉｌ　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｃｏｍｐｉｌｅｒ　　　　　　　　　　　　　　　　　　|
            |　ＥｍｏｊｉＵｔｉｌ　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｅｘｔｒａ．ｅｍｏｊｉ　　　　　　　　　　　　　　　　　　　　|
            |　ＱｒＣｏｄｅＵｔｉｌ　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｅｘｔｒａ．ｑｒｃｏｄｅ　　　　　　　　　　　　　　　　　　　|
            |　ＢｌｏｏｍＦｉｌｔｅｒＵｔｉｌ　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｂｌｏｏｍｆｉｌｔｅｒ　　　　　　　　　　　　　　　　　　　　|
            |　ＢｏｏｌｅａｎＵｔｉｌ　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｕｔｉｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＷａｔｃｈＵｔｉｌ　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｉｏ．ｗａｔｃｈ　　　　　　　　　　　　　　　　　　|
            |　ＧａｎｙｍｅｄＵｔｉｌ　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｅｘｔｒａ．ｓｓｈ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＳｔｙｌｅＵｔｉｌ　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｐｏｉ．ｅｘｃｅｌ．ｓｔｙｌｅ　　　　　　　　　　　　　　　　|
            |　ＳｐｒｉｎｇＵｔｉｌ　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｅｘｔｒａ．ｓｐｒｉｎｇ　　　　　　　　　　　　　　　　　　　|
            |　ＩｎｔｅｒｎａｌＪＳＯＮＵｔｉｌ　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｊｓｏｎ　　　　　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＥｘｃｅｌＥｘｔｒａｃｔｏｒＵｔｉｌ　　　|　ｃｎ．ｈｕｔｏｏｌ．ｐｏｉ．ｅｘｃｅｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＳｐｌｉｔｅｒａｔｏｒＵｔｉｌ　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｃｏｌｌｅｃｔｉｏｎ　　　　　　　　　　　　　　　　|
            |　ＤｂＵｔｉｌ　　　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｄｂ　　　　　　　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＩｎｔｅｒｎａｌＥｓｃａｐｅＵｔｉｌ　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｔｅｘｔ．ｅｓｃａｐｅ　　　　　　　　　　　　　　　|
            |　ＣｅｌｌＵｔｉｌ　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｐｏｉ．ｅｘｃｅｌ．ｃｅｌｌ　　　　　　　　　　　　　　　　　|
            |　ＲｅｆｅｒｅｎｃｅＵｔｉｌ　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｕｔｉｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＳｅｃｕｒｅＵｔｉｌ　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｒｙｐｔｏ　　　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＪＳＯＮＵｔｉｌ　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｊｓｏｎ　　　　　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＷｏｒｄＵｔｉｌ　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｐｏｉ．ｗｏｒｄ　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＤｉｇｅｓｔＵｔｉｌ　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｒｙｐｔｏ．ｄｉｇｅｓｔ　　　　　　　　　　　　　　　　　　|
            |　ＭｅｔａＵｔｉｌ　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｄｂ．ｍｅｔａ　　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＵＲＬＵｔｉｌ　　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｕｔｉｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＭａｐＵｔｉｌ　　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｍａｐ　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＴｈｒｅａｄＵｔｉｌ　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｔｈｒｅａｄ　　　　　　　　　　　　　　　　　　　　|
            |　ＰｒｏｘｙＵｔｉｌ　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ａｏｐ　　　　　　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＲａｎｄｏｍＵｔｉｌ　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｕｔｉｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＭａｉｌＵｔｉｌ　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｅｘｔｒａ．ｍａｉｌ　　　　　　　　　　　　　　　　　　　　　|
            |　ＥｘｃｅｌＤａｔｅＵｔｉｌ　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｐｏｉ．ｅｘｃｅｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＣｌａｓｓＬｏａｄｅｒＵｔｉｌ　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｕｔｉｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＺｉｐＵｔｉｌ　　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｕｔｉｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＯｓｈｉＵｔｉｌ　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｓｙｓｔｅｍ．ｏｓｈｉ　　　　　　　　　　　　　　　　　　　　|
            |　ＳｃｒｅｅｎＵｔｉｌ　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｓｗｉｎｇ　　　　　　　　　　　　　　　　　　　　　|
            |　ＵｎｉｃｏｄｅＵｔｉｌ　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｔｅｘｔ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＸｍｌＵｔｉｌ　　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｕｔｉｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＴｅｍｐｌａｔｅＵｔｉｌ　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｅｘｔｒａ．ｔｅｍｐｌａｔｅ　　　　　　　　　　　　　　　　　|
            |　ＳｅｒｖｌｅｔＵｔｉｌ　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｅｘｔｒａ．ｓｅｒｖｌｅｔ　　　　　　　　　　　　　　　　　　|
            |　ＰｅｍＵｔｉｌ　　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｒｙｐｔｏ　　　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＤａｔａＳｉｚｅＵｔｉｌ　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｉｏ．ｕｎｉｔ　　　　　　　　　　　　　　　　　　　|
            |　ＲｅＵｔｉｌ　　　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｕｔｉｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＥｘｃｅｌＳａｘＵｔｉｌ　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｐｏｉ．ｅｘｃｅｌ．ｓａｘ　　　　　　　　　　　　　　　　　　|
            |　ＨｔｔｐＵｔｉｌ　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｈｔｔｐ　　　　　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＥｘｃｅｌＦｉｌｅＵｔｉｌ　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｐｏｉ．ｅｘｃｅｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＪａｖａＦｉｌｅＯｂｊｅｃｔＵｔｉｌ　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｃｏｍｐｉｌｅｒ　　　　　　　　　　　　　　　　　　|
            |　ＡｒｒａｙＵｔｉｌ　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｕｔｉｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＰａｔｈＵｔｉｌ　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｉｏ．ｆｉｌｅ　　　　　　　　　　　　　　　　　　　|
            |　ＳｅｔｔｉｎｇＵｔｉｌ　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｓｅｔｔｉｎｇ　　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＦｏｎｔＵｔｉｌ　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｉｍｇ　　　　　　　　　　　　　　　　　　　　　　　|
            |　Ｉｐｖ４Ｕｔｉｌ　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｎｅｔ　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＲａｄｉｘＵｔｉｌ　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｕｔｉｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＤｅｓｋｔｏｐＵｔｉｌ　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｓｗｉｎｇ　　　　　　　　　　　　　　　　　　　　　|
            |　ＣｌａｓｓＵｔｉｌ　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｕｔｉｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＣｓｖＵｔｉｌ　　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｔｅｘｔ．ｃｓｖ　　　　　　　　　　　　　　　　　　|
            |　ＶａｌｉｄａｔｉｏｎＵｔｉｌ　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｅｘｔｒａ．ｖａｌｉｄａｔｉｏｎ　　　　　　　　　　　　　　　|
            |　ＷｏｒｋｂｏｏｋＵｔｉｌ　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｐｏｉ．ｅｘｃｅｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＦｉｌｅＴｙｐｅＵｔｉｌ　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｉｏ　　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＣｌｉｐｂｏａｒｄＵｔｉｌ　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｓｗｉｎｇ．ｃｌｉｐｂｏａｒｄ　　　　　　　　　　　|
            |　ＥｎｕｍＵｔｉｌ　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｕｔｉｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＥｘｃｅｌＵｔｉｌ　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｐｏｉ．ｅｘｃｅｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＯｂｊｅｃｔＵｔｉｌ　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｕｔｉｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＴｏｋｅｎｉｚｅｒＵｔｉｌ　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｅｘｔｒａ．ｔｏｋｅｎｉｚｅｒ　　　　　　　　　　　　　　　　|
            |　ＴｅｍｐｏｒａｌＵｔｉｌ　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｄａｔｅ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＫｅｙＵｔｉｌ　　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｒｙｐｔｏ　　　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＣｒｅｄｉｔＣｏｄｅＵｔｉｌ　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｕｔｉｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＥｘｃｅｌＰｉｃＵｔｉｌ　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｐｏｉ．ｅｘｃｅｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＮｉｏＵｔｉｌ　　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｉｏ　　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＦｉｌｅＵｔｉｌ　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｉｏ　　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＥＣＫｅｙＵｔｉｌ　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｒｙｐｔｏ　　　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＲｏｗＵｔｉｌ　　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｐｏｉ．ｅｘｃｅｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＦｉｌｅＮａｍｅＵｔｉｌ　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｉｏ．ｆｉｌｅ　　　　　　　　　　　　　　　　　　　|
            |　ＲｅｆｌｅｃｔＵｔｉｌ　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｕｔｉｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＨｅｘＵｔｉｌ　　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｕｔｉｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＩｄＵｔｉｌ　　　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｕｔｉｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＣｏｌｌｅｃｔｉｏｎＵｔｉｌ　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｃｏｌｌｅｃｔｉｏｎ　　　　　　　　　　　　　　　　|
            |　ＵｓｅｒＡｇｅｎｔＵｔｉｌ　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｈｔｔｐ．ｕｓｅｒａｇｅｎｔ　　　　　　　　　　　　　　　　　|
            |　ＤａｔｅＵｔｉｌ　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｄａｔｅ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＣａｌｅｎｄａｒＵｔｉｌ　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｄａｔｅ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＣｏｍｐａｒｅＵｔｉｌ　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｃｏｍｐａｒａｔｏｒ　　　　　　　　　　　　　　　　|
            |　ＲｏｂｏｔＵｔｉｌ　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｓｗｉｎｇ　　　　　　　　　　　　　　　　　　　　　|
            |　ＬｏｃｋＵｔｉｌ　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｔｈｒｅａｄ．ｌｏｃｋ　　　　　　　　　　　　　　　|
            |　ＣａｐｔｃｈａＵｔｉｌ　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃａｐｔｃｈａ　　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＴａｂｌｅＵｔｉｌ　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｐｏｉ．ｗｏｒｄ　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＭｏｄｉｆｉｅｒＵｔｉｌ　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｕｔｉｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＩｔｅｒＵｔｉｌ　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｃｏｌｌｅｃｔｉｏｎ　　　　　　　　　　　　　　　　|
            |　ＩｎｔｅｒｎａｌＭａｉｌＵｔｉｌ　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｅｘｔｒａ．ｍａｉｌ　　　　　　　　　　　　　　　　　　　　　|
            |　ＴｙｐｅＵｔｉｌ　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｕｔｉｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＳｔａｔｅｍｅｎｔＵｔｉｌ　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｄｂ　　　　　　　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＡｎｎｏｔａｔｉｏｎＵｔｉｌ　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ａｎｎｏｔａｔｉｏｎ　　　　　　　　　　　　　　　　|
            |　ＪｓｃｈＵｔｉｌ　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｅｘｔｒａ．ｓｓｈ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＳｏｃｋｅｔＵｔｉｌ　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｓｏｃｋｅｔ　　　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＤｏｃＵｔｉｌ　　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｐｏｉ．ｗｏｒｄ　　　　　　　　　　　　　　　　　　　　　　　|
            |　ＰａｇｅＵｔｉｌ　　　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｃｏｒｅ．ｕｔｉｌ　　　　　　　　　　　　　　　　　　　　　　|
            |　ＳｃｒｉｐｔＵｔｉｌ　　　　　　　　　　　|　ｃｎ．ｈｕｔｏｏｌ．ｓｃｒｉｐｔ　　　　　　　　　　　　　　　　　　　　　　　　　|
            +－－－－－－－－－－－－－－－－－－－－－－+－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－－+


        * */

    }

    @Test
    public void test2(){
        System.out.println(IdUtil.simpleUUID());
    }

}
