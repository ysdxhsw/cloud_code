package com.hsw.springCloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @description: hystrix熔断器消费端
 * @author: com.hsw
 * @date: 2021/3/14
 */
@EnableEurekaClient
@EnableFeignClients
@SpringBootApplication
@EnableHystrix //开启hystrix熔断降级服务
public class FeignHystrixMain80 {
    public static void main(String[] args) {
        SpringApplication.run(FeignHystrixMain80.class,args);
        System.out.println("hystrix熔断器消费端80启动");
    }
}
