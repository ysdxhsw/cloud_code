package com.hsw.springCloud.service;

import org.springframework.stereotype.Component;

/**
 * @description: 统一进行降级处理
 * @author: com.hsw
 * @date: 2021/3/14
 */
@Component  //让spring容器扫描该类，不要忘记写
public class PaymentFallbackService implements PaymentHystrixService{
    @Override
    public String paymentInfo_OK(Integer id) {
        return "********PaymentFallbackService*****paymentInfo_OK";
    }

    @Override
    public String paymentInfo_TimeOut(Integer id) {
        return "********PaymentFallbackService*****paymentInfo_TimeOut";
    }
}
