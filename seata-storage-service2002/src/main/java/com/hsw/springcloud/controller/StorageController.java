package com.hsw.springcloud.controller;

import com.hsw.springcloud.common.CommonResult;
import com.hsw.springcloud.service.StorageService;
import com.hsw.springcloud.service.impl.StorageServiceImpl;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @description: 库存扣减
 * @author: com.hsw
 * @date: 2021/3/24
 */
@RestController
@RequestMapping("/storage")
public class StorageController {

    @Resource
    private StorageService storageService;


    /*扣减库存*/
    @PostMapping(value = "/decrease")
    //public CommonResult decrease(@RequestParam("productId") Long productId, @RequestParam("count") Integer count){
    public CommonResult decrease( Long productId,  Integer count){
        storageService.decrease(productId,count);
        return new CommonResult(200,"库存扣减成功");

    }


}
