package com.hsw.springcloud.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @description: TODO
 * @author: com.hsw
 * @date: 2021/3/24
 */
@Mapper
public interface StorageDAO {

    /* 扣减库存*/
    void decrease(@Param("productId") Long productId, @Param("count") Integer count);
    //void decrease(@RequestParam("productId") Long productId, @RequestParam("count") Integer count);

}
