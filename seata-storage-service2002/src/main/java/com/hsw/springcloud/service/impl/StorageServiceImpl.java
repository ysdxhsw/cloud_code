package com.hsw.springcloud.service.impl;

import com.hsw.springcloud.dao.StorageDAO;
import com.hsw.springcloud.service.StorageService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @description: TODO
 * @author: hsw
 * @date: 2021/3/24
 */
@Service
public class StorageServiceImpl implements StorageService{

    @Resource
    private StorageDAO storageDAO;

    @Override
    public void decrease(Long productId, Integer count) {
        storageDAO.decrease(productId,count);
    }
}
