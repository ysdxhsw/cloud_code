package com.hsw.springcloud.service;

import org.springframework.web.bind.annotation.RequestParam;

/**
 * @description: TODO
 * @author: hsw
 * @date: 2021/3/24
 */
public interface StorageService {

    void decrease(Long productId, Integer count);
}
