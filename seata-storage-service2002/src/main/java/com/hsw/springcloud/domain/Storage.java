package com.hsw.springcloud.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @description: 库存表
 * @author: com.hsw
 * @date: 2021/3/24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Storage {

    /* 主键*/
    private Long id;

    /* 产品id*/
    private Long productId;

    /* 总库存*/
    private Integer total;

    /* 已用库存*/
    private Integer used;

    /* 剩余库存*/
    private Integer residue;

}
