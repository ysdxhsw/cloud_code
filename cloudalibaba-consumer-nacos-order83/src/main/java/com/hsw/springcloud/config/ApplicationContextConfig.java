package com.hsw.springcloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @description: 远程HTTP服务注入spring容器(httpClient技术)
 * @author: com.hsw
 * @date: 2021/3/18
 */
@Configuration
public class ApplicationContextConfig {

    /**
     * Nacos自带负载均衡，需要加上@LoadBalanced注解，不然会报错：nested exception is java.net.UnknownHostException: nacos-payment-provider] with root cause
     * @return
     */
    @Bean
    @LoadBalanced
    public RestTemplate getRestTemplate(){
        return new RestTemplate();
    }

}
