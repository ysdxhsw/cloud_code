package com.hsw.springcloud.controller;

import com.hsw.springcloud.common.CommonResult;
import com.hsw.springcloud.domain.Order;
import com.hsw.springcloud.service.OrderService;
import com.hsw.springcloud.service.impl.OrderServiceImpl;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @description: 创建订单
 * @author: com.hsw
 * @date: 2021/3/24
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    @Resource
    private OrderServiceImpl orderService;

    @GetMapping("/create")
    public CommonResult create(Order order){
        orderService.create(order);
        return new CommonResult(200,"订单完成",null);
    }

}
