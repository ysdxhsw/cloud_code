package com.hsw.springcloud.dao;

import com.hsw.springcloud.domain.Order;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * @description: TODO
 * @author: com.hsw
 * @date: 2021/3/24
 */
@Mapper
public interface OrderDAO {

    /* 1、新建订单*/
    void create(Order order);
    /* 2、修改订单状态，从0改为1*/
    void update(@Param("userId") Long userId,@Param("status") Integer status );

}
