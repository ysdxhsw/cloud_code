package com.hsw.springcloud;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.jdbc.DataSourceAutoConfiguration;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @description: TODO
 * @author: com.hsw
 * @date: 2021/3/24
 */
@EnableFeignClients
@EnableDiscoveryClient
@SpringBootApplication
//@SpringBootApplication(exclude = DataSourceAutoConfiguration.class)//取消数据源的自动创建，因为要使用seata对数据源代理
public class SeataOrderMainApp2001 {
    public static void main(String[] args) {
        SpringApplication.run(SeataOrderMainApp2001.class,args);
        System.out.println("项目2001启动成功");
    }
}
