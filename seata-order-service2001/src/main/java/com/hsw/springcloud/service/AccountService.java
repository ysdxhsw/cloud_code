package com.hsw.springcloud.service;

import com.hsw.springcloud.common.CommonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

/**
 * @description: OpenFeigen接口调用-- 账户表
 * @author: com.hsw
 * @date: 2021/3/24
 */
@FeignClient(value = "seata-account-service")
public interface AccountService {
    /* 扣减账户余额*/
    @PostMapping(value = "/account/decrease")
    CommonResult decrease(@RequestParam("userId") Long userId, @RequestParam("money") BigDecimal money);
}
