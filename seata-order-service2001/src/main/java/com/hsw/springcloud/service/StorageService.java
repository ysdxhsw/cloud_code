package com.hsw.springcloud.service;

import com.hsw.springcloud.common.CommonResult;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.math.BigDecimal;

/**
 * @description: OpenFeigen接口调用-- 库存表
 * @author: com.hsw
 * @date: 2021/3/24
 */
@FeignClient(value = "seata-storage-service")
public interface StorageService {

    /*扣减库存*/
    @PostMapping(value = "/storage/decrease")
    CommonResult decrease(@RequestParam("productId") Long productId, @RequestParam("count") Integer count);

}
