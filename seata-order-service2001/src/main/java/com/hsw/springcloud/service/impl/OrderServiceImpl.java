package com.hsw.springcloud.service.impl;

import com.hsw.springcloud.dao.OrderDAO;
import com.hsw.springcloud.domain.Order;
import com.hsw.springcloud.service.AccountService;
import com.hsw.springcloud.service.OrderService;
import com.hsw.springcloud.service.StorageService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

/**
 * @description: 创建订单-->扣减库存-->扣减账户金额-->修改订单状态
 * @author: com.hsw
 * @date: 2021/3/24
 */
@Slf4j
@Service
public class OrderServiceImpl implements OrderService {

    @Resource
    private OrderDAO orderDAO;

    @Resource
    private StorageService storageService;

    @Resource
    private AccountService accountService;

    /**
     * 创建订单
     * @param order
     */
    @Override
    public void create(Order order) {

        /* 1 创建订单*/
        orderDAO.create(order);

        /* 2、扣减库存*/
        storageService.decrease(order.getProductId(),order.getCount());

        /* 3、扣减账户*/
        accountService.decrease(order.getUserId(),order.getMoney());

        /* 4、修改状态*/
        orderDAO.update(order.getUserId(),0);


    }

}
