package com.hsw.springcloud.service;

import com.hsw.springcloud.domain.Order;

/**
 * @description: TODO
 * @author: com.hsw
 * @date: 2021/3/24
 */
public interface OrderService {
    /**
     * 创建订单
     * @param order
     */
    void create(Order order);

}
