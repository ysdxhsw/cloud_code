package com.hsw.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description: Spring-cloud-Steam整合Rabbit 消息的发送者
 * @author: com.hsw
 * @date: 2021/3/17
 */
@SpringBootApplication
public class SteamRabbitMQMain8801 {
    public static void main(String[] args) {
        SpringApplication.run(SteamRabbitMQMain8801.class,args);
        System.out.println("Spring-cloud-Steam整合Rabbit启动8801");
    }
}
