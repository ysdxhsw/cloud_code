package com.hsw.springcloud.service;

/**
 * @description: 发送消息接口
 * @author: com.hsw
 * @date: 2021/3/17
 */
public interface IMessageProvider {
    /**
     * 发送消息
     */
    public String send();
}
