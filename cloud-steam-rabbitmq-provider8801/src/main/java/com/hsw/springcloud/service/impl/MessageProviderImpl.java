package com.hsw.springcloud.service.impl;

import cn.hutool.core.util.IdUtil;
import com.hsw.springcloud.service.IMessageProvider;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.messaging.Source;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;

import javax.annotation.Resource;


/**
 * @description: 发送消息实现类
 * @author: com.hsw
 * @date: 2021/3/17
 */
//@Service 这里不需要service,因为这个service是与rabbitmq联系，与以前的增删改查不一样，没有dao
@EnableBinding(Source.class)//定义消息的推送管道
public class MessageProviderImpl implements IMessageProvider {

    @Resource
    private MessageChannel output;//消息发送管道

    /**
     * 发送消息
     * @return
     */
    @Override
    public String send() {

        String serial = IdUtil.simpleUUID();
        output.send(MessageBuilder.withPayload(serial).build());//构建器，引入的是org.springframework.messaging.support
        System.out.println("*********serial"+serial);

        return null;
    }
}
