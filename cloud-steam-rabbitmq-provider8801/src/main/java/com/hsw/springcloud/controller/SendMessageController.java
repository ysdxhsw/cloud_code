package com.hsw.springcloud.controller;

import com.hsw.springcloud.service.IMessageProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @description: 消息发送到rabbitmq上
 * @author: com.hsw
 * @date: 2021/3/17
 */
@Slf4j
@RestController
public class SendMessageController {

    @Resource
    private IMessageProvider messageProvider;

    @GetMapping(value = "/sendMessage")
    public String sendMessage(){
        String send = messageProvider.send();
        log.info("成功向rabbit发送数据");
        return send;
    }

}
