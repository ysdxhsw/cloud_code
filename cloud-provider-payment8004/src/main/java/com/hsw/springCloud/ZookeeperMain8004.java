package com.hsw.springCloud;

import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @description: Zookeeper注册中心
 * @author: com.hsw
 * @date: 2021/3/11
 */

@SpringBootApplication
@EnableDiscoveryClient //该注解用于向使用consul或者zookeeper作为注册中心时注册服务
public class ZookeeperMain8004 {

    public static void main(String[] args) {
        SpringApplication.run(ZookeeperMain8004.class,args);
        System.out.println("");
    }
}
