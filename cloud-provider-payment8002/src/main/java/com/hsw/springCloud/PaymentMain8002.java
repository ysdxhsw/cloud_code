package com.hsw.springCloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @description: 微服务的提供者
 * @author: com.hsw
 * @date: 2021/3/10
 */
@SpringBootApplication
@EnableEurekaClient  //通过注册中心进行访问
public class PaymentMain8002 {
    public static void main(String[] args) {
        SpringApplication.run(PaymentMain8002.class,args);
        System.out.println("微服务的提供者8002启动");
    }
}
