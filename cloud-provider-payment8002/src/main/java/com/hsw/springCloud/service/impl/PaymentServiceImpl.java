package com.hsw.springCloud.service.impl;

import com.hsw.springCloud.domain.Payment;
import com.hsw.springCloud.mapper.PaymentMapper;
import com.hsw.springCloud.service.PaymentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @description: TODO
 * @author: com.hsw
 * @date: 2021/3/10
 */
@Service
public class PaymentServiceImpl implements PaymentService {

    @Autowired
    private PaymentMapper paymentMapper;
    /**
     * 查询订单
     * @param payment
     * @return
     */
    @Override
    public List<Payment> selectPayment(Payment payment) {
        return paymentMapper.selectPayment(payment);
    }

    /**
     * 新增数据
     * @param payment
     * @return
     */
    @Override
    public int create(Payment payment) {
        return paymentMapper.create(payment);
    }
}
