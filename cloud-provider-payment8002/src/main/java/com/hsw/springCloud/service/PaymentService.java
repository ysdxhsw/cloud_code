package com.hsw.springCloud.service;

import com.hsw.springCloud.domain.Payment;

import java.util.List;

/**
 * @description: 支付
 * @author: com.hsw
 * @date: 2021/3/10
 */
public interface PaymentService {
    /**
     * 查询
     * @param payment
     * @return
     */
    public List<Payment> selectPayment(Payment payment);


    public int create(Payment payment);

}
