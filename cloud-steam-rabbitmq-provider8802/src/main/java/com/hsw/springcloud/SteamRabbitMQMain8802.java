package com.hsw.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @description: Spring-cloud-steam-rabbitmq消费端
 * @author: com.hsw
 * @date: 2021/3/17
 */
@SpringBootApplication
public class SteamRabbitMQMain8802 {
    public static void main(String[] args) {
        SpringApplication.run(SteamRabbitMQMain8802.class,args);
        System.out.println("Spring-cloud-steam-rabbitmq消费端8002端口启动");
    }
}
