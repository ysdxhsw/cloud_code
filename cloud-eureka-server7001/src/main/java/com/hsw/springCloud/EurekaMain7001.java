package com.hsw.springCloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.server.EnableEurekaServer;

/**
 * @description: Eureka注册中心
 * @author: com.hsw
 * @date: 2021/3/10
 */
@SpringBootApplication
@EnableEurekaServer  //提供服务注册服务
public class EurekaMain7001 {

    public static void main(String[] args) {
        SpringApplication.run(EurekaMain7001.class,args);
        System.out.println("Eureka注册中心7001启动");
    }
}
