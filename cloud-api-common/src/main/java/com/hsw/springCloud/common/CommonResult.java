package com.hsw.springCloud.common;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * @description: 封装返回消息
 * @author: com.hsw
 * @date: 2021/3/10
 */
@Data
@AllArgsConstructor
//@NoArgsConstructor
public class CommonResult<T> {
    private Integer code;
    private String  message;
    private T       data;

    public CommonResult(Integer code, String message){
        this(code,message,null);
    }
    /* 使用自己写的无参构造方法，注释@NoArgsConstructor，不然会报错“java: 已在类 com.hsw.springCloud.common.CommonResult中定义了构造器 CommonResult()”*/
    public CommonResult(){
    }
}
