package com.hsw.springCloud.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @description: TODO
 * @author: com.hsw
 * @date: 2021/3/10
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Payment implements Serializable {//分布式序列化用的上Serializable
    private Long id;//主键
    private String serial;//支付流水号
}
