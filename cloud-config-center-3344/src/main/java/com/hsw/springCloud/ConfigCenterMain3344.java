package com.hsw.springCloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.config.server.EnableConfigServer;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @description: SpringCloud-config配置
 * @author: com.hsw
 * @date: 2021/3/16
 */
@SpringBootApplication
@EnableConfigServer //config配置服务端
public class ConfigCenterMain3344 {
    public static void main(String[] args) {
        SpringApplication.run(ConfigCenterMain3344.class,args);
        System.out.println("SpringCloud-config配置3344端口启动");
    }
}
