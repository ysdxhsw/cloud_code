package com.hsw.springcloud.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description: 基于Nacos的服务提供者
 * @author: com.hsw
 * @date: 2021/3/17
 */
@RestController
@RequestMapping("/payment")
public class PaymentController {
    @Value("${server.port}")
    private String serverport;

    @GetMapping(value = "/nacos/{id}")
    public String getPayment(@PathVariable("id") Integer id){
        return "nacos,serverpart:"+serverport+"\t id"+id;
    }
}
