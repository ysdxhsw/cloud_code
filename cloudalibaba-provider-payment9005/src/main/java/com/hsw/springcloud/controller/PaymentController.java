package com.hsw.springcloud.controller;

import cn.hutool.core.util.IdUtil;
import com.hsw.springCloud.common.CommonResult;
import com.hsw.springCloud.domain.Payment;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;

/**
 * @description: TODO
 * @author: com.hsw
 * @date: 2021/3/23
 */
@RestController
public class PaymentController {


    @Value("${server.port}")
    private String serverport;


    public static HashMap<Long, Payment> hashMap = new HashMap<>();
    static
    {
       hashMap.put(1L,new Payment(1L, IdUtil.simpleUUID()));
       hashMap.put(2L,new Payment(2L, IdUtil.simpleUUID()));
       hashMap.put(3L,new Payment(3L, IdUtil.simpleUUID()));

    }

    /**
     * 测试
     * @param id
     * @return
     */
    @GetMapping("/payment/{id}")
    public CommonResult<Payment> paymentSQL(@PathVariable("id") Long id){
        Payment payment = hashMap.get(id);
        return new CommonResult(200,"from mysql"+serverport,payment);
    }




}
