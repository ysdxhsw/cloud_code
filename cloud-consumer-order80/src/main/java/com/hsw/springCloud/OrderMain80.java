package com.hsw.springCloud;

import com.hsw.myRule.MySelfRule;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.ribbon.RibbonClient;

/**
 * @description: 微服务的消费端
 * 在启动该微服务时的时候就能去加载我们自定义的Ribbon配置类，从而使配置生效。（下面@RibbonClient）(注释后则自定义规则不生效)
 * @author: com.hsw
 * @date: 2021/3/10
 */
@SpringBootApplication
@EnableEurekaClient
@RibbonClient(name = "CLOUD-PAYMENT-SERVICE", configuration = MySelfRule.class) //Rubbin自定义负载均衡规则
public class OrderMain80 {
    public static void main(String[] args) {
        SpringApplication.run(OrderMain80.class,args);
        System.out.println("微服务的消费端80启动");
    }
}
