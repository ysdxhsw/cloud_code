package com.hsw.springCloud.controller;


import com.hsw.springCloud.common.CommonResult;
import com.hsw.springCloud.domain.Payment;
import com.hsw.springCloud.lb.LoadBalancer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;
import java.net.URI;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @description: 微服务的消费端
 * @author: com.hsw
 * @date: 2021/3/10
 */
@RestController
@RequestMapping("/consumer")
@Slf4j
public class OrderController {

    //public static final String PAYMENT_URL="http://localhost:8001";

    /* 支付服务提供者8001集群环境搭建 */
    // 通过在eureka上注册过的微服务名称调用
    public static final String PAYMENT_URL="http://CLOUD-PAYMENT-SERVICE";

    //远程HTTP服务
    @Resource
    private RestTemplate restTemplate;

    //用于获取微服务中服务名对应下的微服务
    @Resource
    private DiscoveryClient discoveryClient;

    //自己手写的负载均衡轮询算法接口
    @Resource
    private LoadBalancer loadBalancer;

    /**
     * 查询
     * getForObject返回对象是json对象
     * @param payment
     * @return
     */
    @GetMapping("/payment/get")
    public CommonResult<Payment> select(Payment payment){
        log.info("传入的值："+payment);
        /*
        *   RestTemplate rest = new RestTemplate();
            Map<String, String> params = new HashMap<String, String>();
            params.put("s", "hello");
            String url = "http://localhost:8990/drce/hello";
            String s = rest.getForObject(url + "?s={s}", String.class,params);
            System.out.println(s);

            参考地址：https://www.cnblogs.com/zhengy-001/p/11694055.html
            * */
        Map<String, Long> params = new HashMap<String, Long>();
        params.put("id",payment.getId());

        return restTemplate.getForObject(PAYMENT_URL+"/payment/select?id={id}",CommonResult.class,params);
       /* Map<String, Long> params = new HashMap<String, Long>();
        params.put("id",payment.getId());
        Map<String, String> params2 = new HashMap<String, String>();
        params2.put("serial",payment.getSerial());
        //报错
        return restTemplate.getForObject(PAYMENT_URL+"/payment/select?id={id}&serial={serial}",CommonResult.class,params,params2);*/
    }

    /**
     * 插入数据
     * @param payment
     * @return
     */
    @GetMapping("/payment/create")
    public CommonResult<Payment> create(Payment payment){
        log.info("**************插入的数据："+payment);
        return restTemplate.postForObject(PAYMENT_URL+"/payment/create",payment,CommonResult.class);
    }



    /**
     * 查询
     * getForEntity返回的对象是ResponseEntity对象
     * @param payment
     * @return
     */
    @GetMapping("/payment/getForEntity")
    public CommonResult<Payment> getInfo(Payment payment){

        Map<String, Long> params = new HashMap<String, Long>();
        params.put("id",payment.getId());

        /**
         * getForEntity方法
         */
        ResponseEntity<CommonResult> forEntity = restTemplate.getForEntity(PAYMENT_URL + "/payment/select?id={id}", CommonResult.class, params);
        if (forEntity.getStatusCode().is2xxSuccessful()){//2xx表示查询成功
            return forEntity.getBody();//将查询到的数据返回到前端
        }else{
            return new CommonResult(444,"查询失败",null);
        }

    }

    /**
     * 手写轮询算法
     */
    @GetMapping("/payment/lb")
    public String getPaymentLB(){
        /* 获取微服务中服务名是“CLOUD-PAYMENT-SERVICE”下的微服务*/
        List<ServiceInstance> instances = discoveryClient.getInstances("CLOUD-PAYMENT-SERVICE");

        if (instances == null || instances.size() <= 0){
            return null;
        }

        //根据自己手写的负载均衡算法获取要访问的服务实例
        ServiceInstance serviceInstance = loadBalancer.instance(instances);

        //获取访问地址
        URI uri = serviceInstance.getUri();

        return restTemplate.getForObject(uri+"/payment/lb",String.class);

    }



}
