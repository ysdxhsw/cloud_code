package com.hsw.springCloud.lb;

import org.springframework.cloud.client.ServiceInstance;

import java.util.List;

/**
 * @description: 负载均衡算法接口
 * @author: com.hsw
 * @date: 2021/3/13
 */
public interface LoadBalancer {
    /**
     * 获取访问地址
     * @param serviceInstances
     * @return
     */
    ServiceInstance instance(List<ServiceInstance> serviceInstances);
}
