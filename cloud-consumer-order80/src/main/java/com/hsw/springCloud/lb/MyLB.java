package com.hsw.springCloud.lb;

import org.springframework.cloud.client.ServiceInstance;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @description: 手写负载均衡算法
 * @author: com.hsw
 * @date: 2021/3/13
 */
@Component  //Spring容器能扫描到
public class MyLB implements LoadBalancer{

    private AtomicInteger atomicInteger = new AtomicInteger(0);//默认为0

    private static int s;

    /**
     * 获取下标
     * @return
     */
    public final int getAndIncrement(){
        int current;
        int next;
        do {
            current = this.atomicInteger.get();//获取当前值
            next = current > 2147483647 ? 0 :current + 1;//2147483647是Integer的正最大范围值
        }while(!this.atomicInteger.compareAndSet(current,next));
        System.out.println("*******这是第"+next+"次访问");
        return next;
    }

    /**
     * 根据下标获取执行地址
     * @param serviceInstances
     * @return
     */
    @Override
    public ServiceInstance instance(List<ServiceInstance> serviceInstances) {
        int index = getAndIncrement() % serviceInstances.size();
        return serviceInstances.get(index);
    }
}
