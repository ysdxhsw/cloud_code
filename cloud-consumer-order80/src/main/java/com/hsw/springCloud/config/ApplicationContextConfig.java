package com.hsw.springCloud.config;

import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

/**
 * @description: 远程HTTP服务注入spring容器(httpClient技术)
 * @author: com.hsw
 * @date: 2021/3/10
 */
@Configuration
public class ApplicationContextConfig {
    /**
     * 使用@LoadBalanced注解赋予RestTemplate负载均衡的能力
     * @return
     */
    @Bean
    //@LoadBalanced   //因为手写本地负载均衡，把该注解注释掉
    public RestTemplate getRestTemplate(){
        return new RestTemplate();
    }
}
