package com.hsw.myRule;

import com.netflix.loadbalancer.IRule;
import com.netflix.loadbalancer.RandomRule;
import com.netflix.loadbalancer.RoundRobinRule;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @description: 自定义负载均衡路由规则类
 * @author: com.hsw
 * @date: 2021/3/12
 */
@Configuration
public class MySelfRule {
    @Bean
    public IRule myRule(){
        /* 负载均衡规则设置为随机*/
        return new RandomRule();
    }
}
