package com.hsw.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @description: 注册到nacos注册服务中心,基于Nacos的服务提供者
 * @author: com.hsw
 * @date: 2021/3/17
 */
@EnableDiscoveryClient
@SpringBootApplication
public class NacosProviderMain9002 {
    public static void main(String[] args) {
        SpringApplication.run(NacosProviderMain9002.class,args);
        System.out.println("注册到nacos注册服务中心端口9002");
    }
}
