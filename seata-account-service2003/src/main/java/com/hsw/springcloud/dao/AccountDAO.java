package com.hsw.springcloud.dao;

import com.hsw.springcloud.common.CommonResult;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;


import java.math.BigDecimal;

/**
 * @description: TODO
 * @author: hsw
 * @date: 2021/3/24
 */
@Mapper
public interface AccountDAO {

    /* 扣减用户金额*/
    //void decrease(Long userId, BigDecimal money); //会报错：Parameter 'money' not found. Available parameters are [arg1, arg0, param1, param2]
    void decrease(@Param("userId") Long userId, @Param("money") BigDecimal money);
}
