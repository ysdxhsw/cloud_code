package com.hsw.springcloud.controller;

import com.hsw.springcloud.common.CommonResult;
import com.hsw.springcloud.service.AccountService;
import com.hsw.springcloud.service.impl.AccountServiceImpl;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * @description: 扣减用户金额
 * @author: hsw
 * @date: 2021/3/24
 */
@RestController
@RequestMapping("/account")
public class AccoutController {

    @Resource
    private AccountService accountService;


    @PostMapping(value = "/decrease")
    CommonResult decrease(@RequestParam("userId") Long userId, @RequestParam("money") BigDecimal money){
        accountService.decrease(userId,money);
        return new CommonResult(200,"成功扣减用户金额");
    }


}
