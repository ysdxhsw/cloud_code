package com.hsw.springcloud.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @description: 账户表
 * @author: hsw
 * @date: 2021/3/24
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Account {

    /*  主键*/
    private Long id;
    /* 用户ID*/
    private Long userId;
    /* 总额度*/
    private BigDecimal total;
    /* 已用额度*/
    private BigDecimal used;
    /* 剩余可用额度*/
    private BigDecimal residue;

}
