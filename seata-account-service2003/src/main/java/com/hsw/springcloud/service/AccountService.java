package com.hsw.springcloud.service;

import java.math.BigDecimal;

/**
 * @description: TODO
 * @author: hsw
 * @date: 2021/3/24
 */
public interface AccountService {
    void decrease(Long userId, BigDecimal money);
}
