package com.hsw.springcloud.service.impl;

import com.hsw.springcloud.dao.AccountDAO;
import com.hsw.springcloud.service.AccountService;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.math.BigDecimal;

/**
 * @description: TODO
 * @author: hsw
 * @date: 2021/3/24
 */
@Service
public class AccountServiceImpl implements AccountService {
    @Resource
    private AccountDAO accountDAO;

    @Override
    public void decrease(Long userId, BigDecimal money) {
        accountDAO.decrease(userId,money);
    }
}
