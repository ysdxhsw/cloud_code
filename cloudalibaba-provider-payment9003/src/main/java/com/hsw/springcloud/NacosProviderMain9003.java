package com.hsw.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @description: 服务端
 * @author: com.hsw
 * @date: 2021/3/23
 */
@EnableDiscoveryClient
@SpringBootApplication
public class NacosProviderMain9003 {
    public static void main(String[] args) {
        SpringApplication.run(NacosProviderMain9003.class,args);
        System.out.println("服务端9003启动");

    }
}
