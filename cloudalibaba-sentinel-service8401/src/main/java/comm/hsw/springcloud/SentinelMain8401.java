package comm.hsw.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @description: Sentinel熔断与降级服务
 * @author: com.hsw
 * @date: 2021/3/23
 */
@EnableDiscoveryClient
@SpringBootApplication
public class SentinelMain8401 {
    public static void main(String[] args) {
        SpringApplication.run(SentinelMain8401.class,args);
        System.out.println("Sentinel熔断与降级服务8401端口启动了");
    }
}
