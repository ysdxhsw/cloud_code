package comm.hsw.springcloud.controller;

import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.hsw.springCloud.common.CommonResult;
import com.hsw.springCloud.domain.Payment;
import comm.hsw.springcloud.myhandle.CustomerBlockHandler;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @description: Sentinel限流
 * @author: com.hsw
 * @date: 2021/3/23
 */
@RestController
public class RateLimitController {

    /**
     * 1、按资源名称限流
     * @return
     */
    @GetMapping("/byResource")
    @SentinelResource(value = "byResource",blockHandler = "handleException")
    public CommonResult byResource(){
        return new CommonResult(200,"按资源名测试ok",new Payment(2021L,"serial001"));
    }

    /**
     * 兜底方法
     * @param exception
     * @return
     */
    public CommonResult handleException(BlockException exception){
        return new CommonResult(200,exception.getClass().getCanonicalName()+"\t 服务不可用");
    }

    /**
     * 2、按Url限流测试
     * @return
     */
    @GetMapping("/rateLimit/byUrl")
    @SentinelResource(value = "byUrl")
    public CommonResult byUrl(){
        return new CommonResult(200,"按Url测试ok",new Payment(2021L,"serial002"));
    }

    /**
     * 3、全局兜底方法
     * @return
     */
    @GetMapping("/rateLimit/CustomerBlockHandle")
    @SentinelResource(value = "CustomerBlockHandle",
            blockHandlerClass = CustomerBlockHandler.class,//自定义的类
            blockHandler = "handleException")//类中定义的方法
    public CommonResult CustomerBlockHandle(){
        return new CommonResult(200,"全局兜底方法ok",new Payment(2021L,"serial003"));
    }



}
