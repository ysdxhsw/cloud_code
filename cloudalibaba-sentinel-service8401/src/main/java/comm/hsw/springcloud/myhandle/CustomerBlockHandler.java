package comm.hsw.springcloud.myhandle;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.hsw.springCloud.common.CommonResult;

/**
 * @description: 自定义全局兜底方法
 * @author: com.hsw
 * @date: 2021/3/23
 */
public class CustomerBlockHandler {

    public static CommonResult handleException(BlockException exception){
        return new CommonResult(2021,"自定义的限流处理信息.....CustomerBlockHandle");
    }
}
