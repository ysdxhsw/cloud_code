package com.hsw.springCloud.service;

import com.hsw.springCloud.common.CommonResult;
import com.hsw.springCloud.domain.Payment;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.cloud.openfeign.SpringQueryMap;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * @description: TODO
 * @author: com.hsw
 * @date: 2021/3/13
 */
@Component
@FeignClient(value = "CLOUD-PAYMENT-SERVICE")
public interface FeignPaymentService {
    /**
     * 查询
     * @param payment
     * @return
     */
    @GetMapping("/payment/select")
    public CommonResult select(@SpringQueryMap Payment payment);//添加@SpringQueryMap注解可以解决参数不能传递到服务提供端的问题

    @GetMapping(value = "/payment/lb")
    public String getPaymentLB();

    @PostMapping("/payment/create")
    public CommonResult create(@RequestBody Payment payment);


    /**
     * 模拟超时
     * @return
     */
    @GetMapping(value = "/payment/timeOut")
    public String getPaymentTimeOut();

}
