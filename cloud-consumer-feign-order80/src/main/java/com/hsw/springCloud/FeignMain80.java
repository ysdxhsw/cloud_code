package com.hsw.springCloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @description: openFeign
 * @author: com.hsw
 * @date: 2021/3/13
 */
@EnableEurekaClient
@EnableFeignClients
@SpringBootApplication
public class FeignMain80 {
    public static void main(String[] args) {
        SpringApplication.run(FeignMain80.class,args);
        System.out.println("微服务openFeign 80端口启动");
    }
}
