package com.hsw.springCloud.controller;

import com.hsw.springCloud.common.CommonResult;
import com.hsw.springCloud.domain.Payment;
import com.hsw.springCloud.service.FeignPaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * @description: openFeign
 * @author: com.hsw
 * @date: 2021/3/13
 */
@Slf4j
@RestController
@RequestMapping("/consumer")
public class FeignController {

    @Resource
    private FeignPaymentService feignPaymentService;

    @GetMapping("/payment/select")
    public CommonResult getInfo( Payment payment){
        log.info("********************测试1"+payment);
        CommonResult select = feignPaymentService.select(payment);
        log.info("********************测试2"+select);
        return select;
    }


    @GetMapping("/payment/get")
    public String getInfo( ){
        String paymentLB = feignPaymentService.getPaymentLB();
        return paymentLB;
    }

    @PostMapping("/payment/create")
    public CommonResult create(@RequestBody Payment payment){
        log.info("********************测试3"+payment);
        CommonResult paymentCommonResult = feignPaymentService.create(payment);
        return paymentCommonResult;
    }

    /**
     * 模拟超时
     * @return
     */
    @GetMapping(value = "/payment/timeOut")
    public String getPaymentTimeOut(){
        String paymentTimeOut = feignPaymentService.getPaymentTimeOut();
        return paymentTimeOut;
    }

}
