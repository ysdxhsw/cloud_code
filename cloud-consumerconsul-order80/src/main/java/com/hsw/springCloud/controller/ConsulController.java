package com.hsw.springCloud.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * @description: TODO
 * @author: com.hsw
 * @date: 2021/3/12
 */
@Slf4j
@RestController
@RequestMapping("/payment")
public class ConsulController {

    private static final String PAYMENT_URL="http://CONSUL-PROVIDER-PAYMENT";

    @Resource
    private RestTemplate restTemplate;

    @GetMapping("/consumer/consul")
    public String getInfo(){
        log.info("***********");
        return restTemplate.getForObject(PAYMENT_URL+"/payment/consul",String.class);

    }

}
