package com.hsw.springCloud.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * @description: zookeeper微服务消费者
 * @author: com.hsw
 * @date: 2021/3/12
 */
@Slf4j
@RestController
@RequestMapping("/consumer")
public class OrderZkController {

    private static final String INVOKE_URL="http://cloud-provider-payment";

    @Resource
    private RestTemplate restTemplate;

    @GetMapping("/payment/zk")
    public String getInfo(){
        log.info("**********微服务查询数据");
        return  restTemplate.getForObject(INVOKE_URL+"/payment/zk",String.class);
    }


}
