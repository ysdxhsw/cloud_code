package com.hsw.springCloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @description: zookeeper消费者
 * @author: com.hsw
 * @date: 2021/3/12
 */
@SpringBootApplication
@EnableDiscoveryClient //该注解用于向使用consul或者zookeeper作为注册中心时注册服务
public class ZookeeperMain80 {
    public static void main(String[] args) {
        SpringApplication.run(ZookeeperMain80.class,args);
        System.out.println("zookeeper消费者80端口启动了！");
    }
}
