package com.hsw.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

/**
 * @description: Nacos作为配置中心演示
 * @author: com.hsw
 * @date: 2021/3/18
 */
@EnableDiscoveryClient
@SpringBootApplication
public class ConfigNacosMain3377 {
    public static void main(String[] args) {
        SpringApplication.run(ConfigNacosMain3377.class,args);
        System.out.println("Nacos作为配置中心演示端口3377启动");
    }
}
