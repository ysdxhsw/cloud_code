package com.hsw.springCloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;

/**
 * @description: Dashboard服务监控
 * @author: com.hsw
 * @date: 2021/3/15
 */
@SpringBootApplication
@EnableHystrixDashboard  //服务监控
public class HystrixDashboardMain9001 {
    public static void main(String[] args) {
        SpringApplication.run(HystrixDashboardMain9001.class,args);
        System.out.println("Dashboard服务监控9001端口启动");
    }


}
