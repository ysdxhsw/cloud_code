package com.hsw.springcloud.service;

import com.hsw.springCloud.common.CommonResult;
import com.hsw.springCloud.domain.Payment;

import com.hsw.springcloud.myhandle.PaymentFallbackService;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

/**
 * @description: @FeignClient服务接口调用
 * @author: com.hsw
 * @date: 2021/3/23
 */
@FeignClient(value = "nacos-payment-provider",fallback = PaymentFallbackService.class) //兜底回调，fallback = PaymentFallbackService.class
//@FeignClient(value = "nacos-payment-provider")
public interface PaymentService {

    @GetMapping("/payment/{id}")
    public CommonResult<Payment> paymentSQL(@PathVariable("id") Long id);
}
