package com.hsw.springcloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.openfeign.EnableFeignClients;

/**
 * @description: 基于Nacos的服务消费者
 * @author: com.hsw
 * @date: 2021/3/18
 */
@EnableDiscoveryClient
@SpringBootApplication
@EnableFeignClients  //OpenFeign接口调用
public class OrderNacosMain84 {
    public static void main(String[] args) {
        SpringApplication.run(OrderNacosMain84.class,args);
        System.out.println("基于Nacos的服务消费者84端口启动");
    }

}
