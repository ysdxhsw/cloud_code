package com.hsw.springcloud.myhandle;

import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.hsw.springCloud.common.CommonResult;
import com.hsw.springCloud.domain.Payment;
import com.hsw.springcloud.service.PaymentService;
import org.springframework.stereotype.Component;

/**
 * @description: 兜底回调
 * @author: com.hsw
 * @date: 2021/3/23
 */
@Component
public class PaymentFallbackService implements PaymentService {

    @Override
    public CommonResult<Payment> paymentSQL(Long id) {
        return new CommonResult<Payment>(444,"服务降级返回，---PaymentFallbackService",new Payment(id,"errorService"));

    }
}
