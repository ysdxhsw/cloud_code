package com.hsw.springcloud.controller;


import com.alibaba.csp.sentinel.annotation.SentinelResource;
import com.alibaba.csp.sentinel.slots.block.BlockException;
import com.hsw.springCloud.common.CommonResult;
import com.hsw.springCloud.domain.Payment;
import com.hsw.springcloud.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * @description: 部署fallback和blockHandler
 * @author: com.hsw
 * @date: 2021/3/23
 */
@Slf4j//打印日志
@RestController
public class CircleBreakConteoller {

    @Value("${service-url.nacos-user-service}")
    private String serverURL;

    @Resource
    private RestTemplate restTemplate;


    //测试OpenFeign
    @Resource
    private PaymentService paymentService;


    @GetMapping("/Resource/{id}")
    @SentinelResource(value = "gitInfo",fallback = "handlerFallback",blockHandler = "blockhandlerException")
    public CommonResult gitInfo(@PathVariable("id") Long id){

        CommonResult<Payment> result = restTemplate.getForObject(serverURL + "/payment/" + id, CommonResult.class, id);

        if (id == 4){
            throw new IllegalArgumentException("IllegalArgumentException,非法参数异常");
        }else if (result.getData() == null){
            throw new NullPointerException("NullPointerException,该id没有对应记录，空指针异常");
        }
        return result;
    }

    //fallBack   只负责业务异常
    public CommonResult handlerFallback(@PathVariable("id") Long id,Throwable e){
        Payment payment = new Payment(id, null);
        return new CommonResult(444,"兜底内容handlerFallBack,exception内容"+e.getMessage(),payment);
    }

    //blockHandler  只负责sentinel控制台配置违规
    public CommonResult blockhandlerException(@PathVariable("id") Long id, BlockException blockException){
        Payment payment = new Payment(id, null);
        return new CommonResult(445,"blockHandler-sentinel限流，无此流水"+ blockException);
    }


    //测试OpenFeign
    @GetMapping(value = "/consumer/paymentSQL/{id}",produces = "application/json;charset=utf-8")
    //@GetMapping(value = "/consumer/paymentSQL/{id}")  //返回的xml格式数据
    public CommonResult paymentSQL(@PathVariable("id") Long id){
        CommonResult result = paymentService.paymentSQL(id);

        log.info("查询的值",result);
        return result;
    }





}
