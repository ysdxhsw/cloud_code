package com.hsw.springcloud.controller;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import javax.annotation.Resource;

/**
 * @description: TODO
 * @author: com.hsw
 * @date: 2021/3/18
 */
@RestController
public class OrderNacosController {

    //private static final String URL = "http://NACOS-PAYMENT-PRIVATER";
    @Value("${service-url.nacos-user-service}")
    private String serverURL;

    @Resource
    private RestTemplate restTemplate;

    @GetMapping("/payment/nacos/{id}")
    public String getInfo(@PathVariable("id") Integer id){
        return restTemplate.getForObject(serverURL+"/payment/"+id,String.class);
    }
}
