package com.hsw.springCloud.config;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @description: gateway网关路由第二种配置方法（第一种是yml配置）
 * @author: com.hsw
 * @date: 2021/3/16
 */
@Configuration
public class GatewayConfig {

    /**
     * 配置饿了一个路由规则，当访问地址http://localhost:9527/guonei时会自动转发到地址： http://news.baidu.com/guonei
     * @param routeLocatorBuilder
     * @return
     */
    @Bean
    public RouteLocator customRouteLocator(RouteLocatorBuilder routeLocatorBuilder){
        RouteLocatorBuilder.Builder routes = routeLocatorBuilder.routes();

        routes.route("path_route_hsw",
                r ->r.path("/guonei").
                        uri("http://news.baidu.com/guonei")).build();
        return routes.build();
    }
}
