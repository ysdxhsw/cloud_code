package com.hsw.springCloud.filter;

import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import java.util.Date;

/**
 * @description: 全局自定义过滤器(Spring-Cloud-Filter过滤器)
 * @author: com.hsw
 * @date: 2021/3/16
 */
@Component  //要被spring扫描到，先把这个注解加上去
@Slf4j      //日志
public class MyLogGateWayFilter implements GlobalFilter, Ordered {//实现GlobalFilter, Ordered 这两个接口

    /**
     * 全局自定义过滤器：过滤效果，访问的url需要有uname=xxx,不然报错
     * @param exchange
     * @param chain
     * @return
     */
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {

        /**
         * 过滤效果：访问的url需要有uname=xxx,不然报错
         */
        log.info("come in global filter: {}", new Date());

        ServerHttpRequest request = exchange.getRequest();

        String uname = request.getQueryParams().getFirst("uname");
        if (uname == null) {
            log.info("用户名为null，非法用户");
            exchange.getResponse().setStatusCode(HttpStatus.NOT_ACCEPTABLE);
            return exchange.getResponse().setComplete();
        }
        // 放行
        return chain.filter(exchange);

    }

    /**
     * 过滤器加载的顺序 越小,优先级别越高
     * @return
     */
    @Override
    public int getOrder() {
        return 0;//优先级：数字越小，优先级越高
    }
}
