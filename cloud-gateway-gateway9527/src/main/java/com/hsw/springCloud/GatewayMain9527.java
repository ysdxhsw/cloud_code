package com.hsw.springCloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @description: gateway新一代网关
 * @author: com.hsw
 * @date: 2021/3/16
 */
@SpringBootApplication
@EnableEurekaClient    //注册进Eureka服务中心
public class GatewayMain9527 {
    public static void main(String[] args) {
        SpringApplication.run(GatewayMain9527.class,args);
        System.out.println("gateway新一代网关9527端口启动");
    }
}
