package com.hsw.springCloud;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

/**
 * @description: SpringCloud-config 配置中心使用
 * @author: com.hsw
 * @date: 2021/3/16
 */
@SpringBootApplication
@EnableEurekaClient
public class ConfigClient3355 {
    public static void main(String[] args) {
        SpringApplication.run(ConfigClient3355.class,args);
        System.out.println("SpringCloud-config 配置中心使用3355端口启动");
    }
}
