package com.hsw.springCloud.mapper;

import com.hsw.springCloud.domain.Payment;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * @description: TODO
 * @author: com.hsw
 * @date: 2021/3/10
 */
@Mapper
public interface PaymentMapper {
    /**
     * 查询
     * @param payment
     * @return
     */
    public List<Payment> selectPayment(Payment payment);

    /**
     * 插入数据
     * @param payment
     * @return
     */
    public int create(Payment payment);
}
