package com.hsw.springCloud.controller;

import com.hsw.springCloud.common.CommonResult;
import com.hsw.springCloud.domain.Payment;
import com.hsw.springCloud.service.PaymentService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cloud.client.ServiceInstance;
import org.springframework.cloud.client.discovery.DiscoveryClient;
import org.springframework.cloud.client.hypermedia.DiscoveredResource;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @description: TODO
 * @author: com.hsw
 * @date: 2021/3/10
 */
@RestController
@RequestMapping("/payment")
@Slf4j//打印日志
public class testController {

    @Autowired
    //@Resource  注意与Autowired的区别
    private PaymentService paymentService;

    /**
     * 服务发现 获取服务信息
     */
    @Resource
    private DiscoveryClient discoveryClient;


    /**
     * 端口号，查看负载均衡效果
     */
    @Value("${server.port}")
    private String serverPort;


    /**
     * 查询
     * @param payment
     * @return
     */
    @GetMapping("/select")
    public CommonResult select(Payment payment) {
        log.info("**********传入的参数:"+payment);
        List<Payment> payments = paymentService.selectPayment(payment);
        log.info("**********查询的结果:"+payments);

        System.out.println("热部署测试");
        if (payments != null){
            return new CommonResult(200,"查询数据成功,serverPort:"+serverPort,payments);
        }else{
            return new CommonResult(444,"查询数据失败,serverPort:"+serverPort,null);
        }
    }

    /**
     * 查询  （为测试gateway添加该方法）
     * @param payment
     * @return
     */
    @GetMapping("/get")
    public CommonResult get(Payment payment) {
        log.info("**********传入的参数:"+payment);
        List<Payment> payments = paymentService.selectPayment(payment);
        log.info("**********查询的结果:"+payments);

        System.out.println("热部署测试");
        if (payments != null){
            return new CommonResult(200,"查询数据成功,serverPort:"+serverPort,payments);
        }else{
            return new CommonResult(444,"查询数据失败,serverPort:"+serverPort,null);
        }
    }


    @PostMapping("/create")
    public CommonResult create(@RequestBody Payment payment){
        log.info("**************插入的数据："+payment);
        int result = paymentService.create(payment);
        log.info("**********插入的结果:"+result);
        if (result > 0){
            return new CommonResult(200,"插入数据成功serverPort:"+serverPort,result);
        }else{
            return new CommonResult(444,"插入数据失败serverPort:"+serverPort,null);
        }
    }


    /**
     * 暴露接口，可以进行服务发现 获取服务信息
     * @return
     */
    @GetMapping("/discovery")
    public Object discovery(){
        /**
         * 查询注册到Eureka上的微服务列表
         */
        List<String> services = discoveryClient.getServices();
        for (String serviceName:services) {
            log.info("*******注册到Eureka上的微服务名字："+serviceName);
        }

        /***
         * 一个微服务下的全部实例
         */
        List<ServiceInstance> instances = discoveryClient.getInstances("CLOUD-PAYMENT-SERVICE");
        for (ServiceInstance instance : instances) {
            log.info(instance.getServiceId()+"\t"+instance.getHost()+"\t"+instance.getPort()+"\t"+instance.getUri());
        }

        return this.discoveryClient;

    }

    /**
     * 手写负载均衡算法
     * @return
     */
    @GetMapping(value = "/lb")
    public String getPaymentLB() {
        return serverPort;
    }


    /**
     * 模拟超时故障
     * @return
     */
    @GetMapping(value = "/timeOut")
    public String getPaymentTimeOut() {
        //睡眠3s
        try {
            TimeUnit.SECONDS.sleep(3);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return serverPort;
    }





}
